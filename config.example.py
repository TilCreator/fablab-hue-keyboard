import logging

log_level = logging.INFO

name = 'hue-keyboard-bridge'

broker_ip = '10.42.0.1'
hue_bridge_ip = '10.42.0.100'
hue_bridge_token = None

broker_user = 'openhabian'
broker_pwd = 'TODO'

mqtt_keyboard_topic = '/HabLab/WZ/8/Key/Code'

hue_room = 'Wohnzimmer'
hue_prefix = 'KeyPad'

brightness_delta = 0.8
colortemp_delta = 0.8

transition_time_zero_key = 5

disco_union = False
disco_init_brightness = 140
disco_random_brightnes = False
disco_init_saturation = 255
disco_random_saturation = False
disco_delay = 10

mqtt_hebel_topic = '/HabLab/WZ/9/hebel/State'
mqtt_hebel_address = 'krasserlichtschalter'
hebel_gpio = 4
hebel_brightness = 1023
