# Hue Keyboard
## Install
- Install `python`, `python-venv`
- Create `venv` with `pytohn -m venv venv`
- Install modules with `venv/bin/pip install -r requirenments.txt`
- Copy config from `config.example.py` to `config.py`
- Change values und `config.py`
- Copy `key-hue-bridge.service` to `/etc/systemd/system/`
- Enable and start service:
```
# systemctl daemon-reload
# systemctl enable key-hue-bridge.service
# systemctl start key-hue-bridge.service
```
## Notes
### Typical MQTT messages
```
/WZ_KeyPad/status Connected
/HabLab/WZ/8/Key/Code 0


/HabLab/WZ/8/Key/Code 82
/HabLab/WZ/8/Key/Code 0


/WZ_KeyPad/status Connection Lost
```
# KEY CODES
```
NUM 49
/   50
*   51
-   67
7   97
8   98
9   99
4   81
5   82
6   83
1   17
2   18
3   19
0   34
+   100
DEL 35
ENT 20
REL 0
```
