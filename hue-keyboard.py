#!/bin/env python3
from phue import Bridge
import paho.mqtt.client as mqtt
import config
import logging
import string
import random
import time
import threading


def on_connect(client, userdata, flags, rc):
    c.subscribe(config.mqtt_keyboard_topic)
    c.subscribe(config.mqtt_hebel_topic)


def on_message(client, userdata, msg):
    global disco_active, disco_thread

    logging.debug(msg.topic + ' ' + str(msg.payload))

    if msg.topic == config.mqtt_keyboard_topic:
        keycodes = {
            49: 'NUM',
            50: '/',
            51: '*',
            67: '-',
            97: '7',
            98: '8',
            99: '9',
            81: '4',
            82: '5',
            83: '6',
            17: '1',
            18: '2',
            19: '3',
            34: '0',
            100: '+',
            35: 'DEL',
            20: 'ENT',
            0: 'REL'
        }
        keycode = keycodes[int(msg.payload)]

        logging.info('Keycode ' + keycode)

        # TODO Add keyboard bashing blocker?

        if disco_active and keycode not in ['NUM', '+', '-', '*', '/', 'REL']:
            disco_active = False

            disco_thread.join()
            disco_thread = None

        if keycode == '0':
            apply_to_group(off=True, transition_time=config.transition_time_zero_key)
        elif keycode in string.digits:
            b.run_scene(config.hue_room, config.hue_prefix + keycode)
        elif keycode == '+':
            apply_to_group(brightness_delta=1 / config.brightness_delta)
        elif keycode == '-':
            apply_to_group(brightness_delta=config.brightness_delta)
        elif keycode == '*':
            apply_to_group(colortemp_delta=config.colortemp_delta)
        elif keycode == '/':
            apply_to_group(colortemp_delta=1 / config.colortemp_delta)
        elif keycode == 'DEL':
            apply_to_group(off=True)
        elif keycode == 'ENT':
            b.run_scene(config.hue_room, config.hue_prefix + 'ENT', transition_time=0)
        elif keycode == 'NUM':
            if disco_active:
                disco_active = False

                disco_thread.join()
                disco_thread = None
            else:
                disco_active = True

                disco_thread = threading.Thread(target=smooth_disco)
                disco_thread.start()

        brightness = config.hebel_brightness if b.get_group(config.hue_room)['state']['any_on'] else 0
        c.publish('{}/cmd'.format(config.mqtt_hebel_address), 'PWM,{},{}'.format(config.hebel_gpio, brightness))

    elif msg.topic == config.mqtt_hebel_topic:
        room_state = b.get_group(config.hue_room)['state']['any_on']

        if room_state and disco_active:
            disco_active = False

            disco_thread.join()
            disco_thread = None

        b.set_group(config.hue_room, {'on': not room_state})

        brightness = config.hebel_brightness if not room_state else 0
        c.publish('{}/cmd'.format(config.mqtt_hebel_address), 'PWM,{},{}'.format(config.hebel_gpio, brightness))


def on_publish(client, userdata, results):
    logging.debug('publish: {}, {}'.format(userdata, results))


def apply_to_group(group_name=config.hue_room, brightness_delta=1, colortemp_delta=1, transition_time=0, off=False):
    for id in [int(id) for id in b.get_group(group_name)['lights']]:
        if off:
            b.set_light(id, {'on': False}, transitiontime=transition_time * 10)
        else:
            light = b.get_light(id)

            light['state']['bri'] = max(0, min(255, int(max(1, light['state']['bri']) * brightness_delta)))
            light['state']['ct'] = max(145, min(500, int(light['state']['ct'] * colortemp_delta)))

            b.set_light(id, {'bri': light['state']['bri'], 'ct': light['state']['ct']}, transitiontime=transition_time * 10)


def smooth_disco(group_name=config.hue_room, init_brightness=config.disco_init_brightness, random_brightness=config.disco_random_brightnes,
                 init_saturation=config.disco_init_saturation, random_saturation=config.disco_random_saturation, delay=config.disco_delay,
                 union=config.disco_union):
    ids = [int(id) for id in b.get_group(group_name)['lights']]

    logging.debug(ids)

    init_data = {'on': True}

    if init_brightness is not None:
        init_data['bri'] = init_brightness

    if init_saturation is not None:
        init_data['sat'] = init_saturation

    b.set_light(ids, init_data, transitiontime=delay * 10 / 10)

    while disco_active:
        for id in ids:
            data = {'hue': int(random.random() * 65535)}

            if random_brightness:
                data['bri'] = int(random.random() * 255)
            if random_saturation:
                data['sat'] = int(random.random() * 255)

            if union:
                b.set_light(ids, data, transitiontime=delay * 10)
            else:
                b.set_light(id, data, transitiontime=delay * 10)

            if union:
                time.sleep(delay)
                break

            time.sleep(delay/len(ids))

            if not disco_active:
                break


logging.basicConfig(level=config.log_level)


c = mqtt.Client(config.name)
c.username_pw_set(username=config.broker_user, password=config.broker_pwd)
c.on_connect = on_connect
c.on_message = on_message
c.on_publish = on_publish
c.connect(config.broker_ip)

b = Bridge(config.hue_bridge_ip, username=config.hue_bridge_token)


disco_active = False
disco_thread = None


try:
    c.loop_forever()
except KeyboardInterrupt:
    if disco_active:
        disco_active = False
        disco_thread.join()

